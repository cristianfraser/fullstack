## Modelo de datos

En archivo `schema.png`
![](schema.png)

## SQL

Considerando el enunciado anterior conteste las siguientes preguntas:

1. Escriba una Query que entregue la lista de  alumnos para el curso
"programación"

```
SELECT
  S.name
FROM
  student S, course_student SC, course C
WHERE
  C.name = "Programacion" AND SC.course_id = C.id AND SC.student_id = S.id
```

2. Escriba una Query  que calcule el promedio de notas de un alumno en un
curso.

Teniendo los IDs del alumno y del curso:

```
SELECT
  S.name, C.name, AVG(ST.grade)
FROM
  student S, course C, student_test ST
WHERE
  S.id = 1 AND C.id = 1 AND S.id = ST.student_id;
```

Teniendo los nombres del alumno y del curso:

```
SELECT
  S.name, C.name, AVG(ST.grade)
FROM
  student S, course C, student_test ST
WHERE
  S.name = "Vincent" AND C.name = "Programacion" AND S.id = ST.student_id;
```

3. Escriba una Query que entregue a los alumnos y el promedio que tiene
en cada curso.

```
SELECT
  S.name, C.name, AVG(ST.grade)
FROM
  student S, course C, student_test ST, test T
WHERE
  S.id = ST.student_id AND ST.test_id = T.id AND T.course_id = C.id
GROUP BY
  ST.student_id, C.name;
```

4. Escriba una Query que lista a todos los alumnos con más de un curso con
promedio rojo.

```
SELECT
  S.name
FROM
  student S, course C, student_test ST, test T
WHERE
  S.id = ST.student_id AND ST.test_id = T.id AND T.course_id = C.id 
GROUP BY
  ST.student_id
HAVING
  COUNT(C.name) > 1 AND AVG(ST.grade) < 4.0;
```

5. Dejando de lado el problema del cólegio se tiene una tabla con información de jugadores de tenis:
`PLAYERS(Nombre, Pais, Ranking)`. Suponga que Ranking es un número de 1 a
100 que es distinto para cada jugador. Si la tabla en un momento dado
tiene
solo 20 registros, indique cuantos registros tiene la tabla que resulta de la
siguiente consulta:

```
SELECT c1.Nombre, c2.Nombre
FROM PLAYERS c1, PLAYERS c2
WHERE c1.Ranking > c2.Ranking
```
Seleccione las respuestas correctas:

```
a) 400
b) 190
c) 20
d) imposible saberlo
```

La respuesta es:

```
b) 190

Porque al hacer el JOIN de la tabla consigo misma, aumentan los registros en
orden n^2, teniendo 400 combinaciones. De esas n=20 son iguales entre sí, de 
las otras 380, la mitad de de los registros de una columna es mayor a los de
la otra (o también podría verse al revés)
```

## Diseño de software

### Backend

1. Clonar el repositorio `git clone repo`
2. Entrar `cd repo`
3. Crear un virtualenv (se asume instalado) `virtualenv -p python3 venv`
4. Instalar dependencias `pip install -r requirements.txt`
5. Entrar a carpeta `prueba`.  `cd prueba`
6. Correr el servidor `python manage.py runserver` 

Para ver la lista de alumnos en el navegador: [`http://127.0.0.1:8000/kcursos/students/`](127.0.0.1:8000/kcursos/students/)

Para la lista de almunos con más de un promedio rojo: [`http://127.0.0.1:8000/kcursos/students/red`](127.0.0.1:8000/kcursos/students/red)

Para el admin crud se tienen las siguientes consideraciones:
- El sitio de admin de django es suficiente para manejar las CRUD básicas.
- Se implementó una manera más amigable para el usuario de agregar una prueba. Teniendo tablas populadas de estudiantes y cursos,
se entrega una página para crear pruebas [`http://127.0.0.1:8000/kadmin/tests/add/`](http://127.0.0.1:8000/kadmin/tests/add/), que permite
especificar el nombre de la prueba nueva y a qué curso pertenece. Una vez creada la prueba, redirige a una nueva página para agregar las notas
de los alumnos que son parte de ese curso.

7. Para poder usar el admin de Django, se tiene que crear un superusuario.
8. Para poder crear CRUD para cada modelo más simple (Student, Teacher), se pueden usar `ModelForm` que genera un formulario automaticamente para 
cada modelo, con el que se pueden crear y editar entradas facilmente. Y para borrar bastaria tener la lista con la opcion de borrar, que tome el `id`
deseado para borrarlo de la DB (o usar el admin).
9. Para crear entradas en `TestStudent` era mejor crear la vista aparte del admin, porque si no se tendria que ingresar manualmente todas las entradas
para los valores `(test_id, student_id, grade)` a mano, en vez de ingresar solamente `(student_id, grade)` a través del formulario custom.

### Frontend

Se tiene que abrir el archivo `frontend/calendario.html` en el navegador para poder ver el calendario. Se usa un JSON hardcodeado.
from django import forms
from django.forms import ModelForm
from kcursos.models import Student, Test, TestStudent, Course

class TestForm(ModelForm):
  class Meta:
    model = Test
    fields = ['title', 'course']

class TestStudentForm(forms.Form):

  def __init__(self, *args, **kwargs):
    self.test_id = kwargs.pop('test_id')
    super(TestStudentForm, self).__init__(*args, **kwargs)

    t = Test.objects.get(id = self.test_id)

    students = Course.objects.values('students__id', 'students__name').filter(id=t.course.id)

    for s in students:
      self.fields[str(s['students__id'])] = forms.DecimalField(label= str(s['students__name']), max_digits=2, decimal_places=1)

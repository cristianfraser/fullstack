from django.conf.urls import url

from . import views

urlpatterns = [
  url(r'^tests/add/$', views.create_test, name='create_test'),
  url(r'^tests/add/grades/(?P<test_id>\d+)/$', views.add_grades, name='add_grades'),
  url(r'^success/$', views.success, name='success'),
]
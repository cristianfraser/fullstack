from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from kcursos.models import Student, Test, TestStudent, Course
from .forms import TestForm, TestStudentForm
from django.urls import reverse
from django.db.models import Count, Avg, F, Case, When

def admink(request):
  pass

def is_number(s):
  try:
    float(s)
    return True
  except ValueError:
    return False

def create_test(request):
  if request.method == 'POST':
    form = TestForm(request.POST)
    if form.is_valid():
      test = form.save()
      response = HttpResponseRedirect(reverse('add_grades', kwargs = { 'test_id': test.id }))
      return response
  else:
    form = TestForm()

  return render(request, 'kadmin/create_test.html', {'form': form})

def add_grades(request, test_id):
  if request.method == 'POST':
    a = []

    for k,v in request.POST.items():
      if is_number(k):
        test = Test.objects.get(id = test_id)
        student = Student.objects.get(id = int(k))
        ts = TestStudent(test = test, student = student, grade = float(v))
        ts.save()

    response = HttpResponseRedirect(reverse('success'))
    return response
  else:
    form = TestStudentForm(test_id= test_id)

  return render(request, 'kadmin/add_grades.html', {'form': form, 'r': request, 'test_id': test_id})

def success(request):
  return render(request, 'kadmin/success.html')


from django.db import models

class Student(models.Model):
  name = models.CharField(max_length=200)

  def __str__(self):
   return self.name

class Teacher(models.Model):
  name = models.CharField(max_length=200)

  def __str__(self):
   return self.name

class Course(models.Model):
  name = models.CharField(max_length=200)
  students = models.ManyToManyField(Student)
  teacher = models.ForeignKey(Teacher)

  def __str__(self):
   return self.name

class Test(models.Model):
  title = models.CharField(max_length=200)
  course = models.ForeignKey(Course)

  def __str__(self):
   return self.title

class TestStudent(models.Model):
  test = models.ForeignKey(Test)
  student = models.ForeignKey(Student)
  grade = models.DecimalField(max_digits=2, decimal_places=1)

  def __str__(self):
    return self.test.title + " " + str(self.student) + " " + str(self.grade)
from django.contrib import admin
from kcursos.models import Student, Teacher, Course, Test, TestStudent

admin.site.register(Student)
admin.site.register(Teacher)
admin.site.register(Course)
admin.site.register(Test)
admin.site.register(TestStudent)
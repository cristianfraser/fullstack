from django.shortcuts import render

from django.http import HttpResponse
from django.template import loader

from .models import Student, Test, TestStudent
from django.db.models import Count, Avg, F, Case, When


def index(request):
  return HttpResponse("Hello, world. You're at the K-Cursos index.")


def get_promedios():
  test_students = TestStudent.objects.all()

  # calculamos el promedio de cada alumno para cada ramo
  return TestStudent.objects.values('student').annotate(c = F('test__course'), c_avg=Avg('grade'))

def all_students(request):
  students = Student.objects.values('id', 'name')
  promedios = get_promedios()
  r = []

  # calculamos el promedio general de cada alumno
  for student in students:
    q = promedios.filter(student=student['id']).values('student__name', 'c_avg').aggregate(avg=Avg('c_avg'))
    s = {'name': student['name'], 'average': q['avg']}

    r.append(s)

  template = loader.get_template('kcursos/all_students.html')
  context = {
    'students': r,
  }
  return HttpResponse(template.render(context, request))

def red_students(request):
  students = Student.objects.values('id', 'name')
  promedios = get_promedios()
  r = []

  # contamos promedios rojos de cada alumno
  for student in students:
    q = promedios.filter(student=student['id'], c_avg__lt = 4).values('student__name', 'c_avg') \
      .aggregate(c=Count('c_avg'))
    
    if q['c'] > 1:
      s = {'name': student['name']}
      r.append(s)


  template = loader.get_template('kcursos/red_students.html')
  context = {
    'students': r,
  }
  return HttpResponse(template.render(context, request))
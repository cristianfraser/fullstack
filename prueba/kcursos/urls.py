from django.conf.urls import url

from . import views

urlpatterns = [
  url(r'^students/$', views.all_students, name='all_students'),
  url(r'^students/red$', views.red_students, name='red_students'),
]
var colors = ['lightblue', 'ivory', 'lightcyan', 'lightgoldenrodyellow', 'lightyellow']

function loadCalendar(events) {
  var days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
  var time_div = document.getElementById('time')

  for(var h = 8 ; h < 20 ; h++){
    var min = ['00', '30']
    min.forEach(function(m) {
      var block = document.createElement('div');
      block.setAttribute('class', 'time-border');
      block.innerHTML = '' + h + ':' + m;
      time_div.appendChild(block);
    })
  }

  days.forEach(function(day, day_i) {
    if(events[day] !== undefined){
      var time_i = [8,0]
      var day_div = document.getElementById(day)

      events[day].forEach(function(e, ev_i) {
        var start_time = e.start_time.split(":").map(function(x) {return parseInt(x)});
        var end_time = e.end_time.split(":").map(function(x) {return parseInt(x)});
        var name = e.name;
        var t = [0,0];

        if(time_i[1] > start_time[1]) {
          t[1] = 30;
          t[0] =  start_time[0] - time_i[0] - 1;
        } else {
          t[1] = start_time[1] - time_i[1];
          t[0] =  start_time[0] - time_i[0];
        }

        var offset = t[1] > 0 ? t[0]*2 + 1 : t[0]*2;

        if(start_time[1] > end_time[1]) {
          t[1] = 30;
          t[0] =  end_time[0] - start_time[0] - 1;
        } else {
          t[1] = end_time[1] - start_time[1];
          t[0] =  end_time[0] - start_time[0];
          //console.log(end_time[0], start_time[0])
        }

        var size = t[1] > 0 ? t[0]*2 + 1 : t[0]*2;

        console.log(offset, size, start_time, end_time, t);

        for(var i = 0 ; i < offset ; i++){
          var block = document.createElement('div');
          block.setAttribute('class', 'time-block');
          day_div.appendChild(block);
        }

        var block = document.createElement('div');
        block.setAttribute('class', 'time-block '+ colors[(day_i+ev_i)%colors.length]);
        block.innerHTML = name;
        day_div.appendChild(block);

        for(var i = 1 ; i < size ; i++){
          var block = document.createElement('div');
          block.setAttribute('class', 'time-block '+colors[(day_i+ev_i)%colors.length]);
          day_div.appendChild(block);
        }


        time_i = end_time;

      })
    }
  })
}


var ev = {
    "monday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "11:00"},
        {"name": "Cristian", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "tuesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "11:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ],
    "wednesday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Cristian", "start_time": "10:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Cristian", "start_time": "17:00", "end_time": "19:30"}
    ],
    "thursday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Cristian", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "18:00"}
    ],
    "friday": [
        {"name": "Jorge", "start_time": "08:00", "end_time": "09:00"},
        {"name": "Jorge", "start_time": "09:30", "end_time": "12:00"},
        {"name": "Jorge", "start_time": "15:00", "end_time": "16:00"},
        {"name": "Jorge", "start_time": "17:00", "end_time": "19:30"}
    ]
}

loadCalendar(ev)